var app = angular.module('htmltocanvas', []);


app.controller('mainCtrl', function($scope, $http){	
	$scope.loading = false
	$scope.date_required = false
	$scope.name_required = false
	$scope.make_it = function() {
	console.log($scope.validity_date)
	console.log($scope.client_name)
	var v_date = moment($scope.validity_date).format("DD MMM YYYY")
		if($scope.validity_date !== undefined && $scope.client_name !== undefined && $scope.client_name !== '' && $scope.validity_date !== ''){
			$scope.loading = true
			var random_six = Math.floor(100000 + Math.random() * 900000)
			document.getElementById('uid').innerHTML = random_six
			html2canvas(document.querySelector("#htmltocanvas")).then(canvas => {
				// document.body.appendChild(canvas)
				var params = {name: $scope.client_name, validity: String(v_date), id: random_six}
				console.log(params)
				$http({
					method: 'POST',
					url: 'https://hooks.zapier.com/hooks/catch/740641/zptanf/',
					data: params,
					headers : {
				        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
				    }
				})
				canvas.toBlob(function(blob) {
		          saveAs(blob, string_to_slug($scope.client_name)+"-"+random_six+".png"); 
		          $scope.loading = false
		          $scope.client_name = ''
		          $scope.validity_date = ''
		        });
			});
		}else {
			if($scope.validity_date === undefined){
				$scope.date_required = true
			}else {
				$scope.date_required = false
			}
			if ($scope.client_name === undefined) {
				$scope.name_required = true				
			}else {
				$scope.name_required = false				
			}
		}
	}

	$scope.getDateFormat = function(date) {
		return moment(date).format("DD MMM YYYY");
	}
})

app.directive('validDate', function(){
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attr, ctrl){
			function vaildateDateFormat(dateValue) {
				if(dateValue.length === 5) {
					var y = new Date()
					scope.validity_date = dateValue + '/' + y.getFullYear()
				}
			}
			ctrl.$parsers.push(vaildateDateFormat);
		}
	}
})

function string_to_slug (str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
  
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}